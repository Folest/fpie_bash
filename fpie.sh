#!/bin/bash

# Author           : Karol Beresniewicz ( karolbe22@gmail.com )
# Created On       : 06.06.2018
# Last Modified By : author ( karolbe22@gmail.com )
# Last Modified On : 11.06.2018
# Version          : 1.0
#
# Description      : Simple tool to represent sizes of file in specified directory in form of chart.
#
# Licensed under GPL (see /usr/share/common-licenses/GPL for more details
# or contact # the Free Software Foundation for a copy)


VERSION="v1.0"

function map_arguments()
{
	ARG_RECURSIVE=
	ARG_DIRECT=
	ARG_GUI=
	ARG_BG=
	ARG_HELP=
	ARG_VERSION=
	ARG_GUI_PACK=
	ARG_BROWSER=
    ARG_POSITIONAL=()
    while [[ $# -gt 0 ]]
    do
	key="$1"
	# echo "$1"
	case $key in
	    -d|--directory)
			ARG_DIRECT="$2"
			shift
			shift
		;;
		-r|--recursive)
			ARG_RECURSIVE="Y"
			shift
		;;
		-g|--gui)
			ARG_GUI="Y"
			shift
		;;
			--yad)
			ARG_GUI_PACK="yad"
			shift
			;;
			--zenity)
			ARG_GUI_PACK="zenity"
			shift
			;;
		-h|--help)
			ARG_HELP="Y"
			shift
		;;
		-b|--browser)
			ARG_BROWSER="$2"
			shift
			shift
		;;
		-v|--version)
			ARG_VERSION="Y"
			shift
		;;

	    *)
			ARG_POSITIONAL+=("$1")
			shift
		;;
	esac
    done
    set -- "${ARG_POSITIONAL[@]}"    
}

function help()
{
	echo "Options :"
	echo "-d or --directory DIRECTORY	- directory on which contents the resulting chart is dependent"
	echo "-g or --gui- with this atribute the directory argument is ignored and base directory is based on result of dialog default is yad" 
		echo "--zenity	- use zenity as gui" 
		echo "--yad	- use yad as gui"
	echo "-b BROWSER-PACKAGE - specify which browser is supposed to open the resulting chart"
	echo "-r or --recursive - with this attribute the result is based on files rather than on directory sizes"
	echo "-h or - help "
	echo "-v or - version "
}

# First argument is formatted string to be places inside the data.AddRows fun
function create_chart()
{
	TEMP=$(mktemp -t chart.XXXXX)
	QUERY1=36
	NAME1='File 1 size'
	QUERY2=64
	cat > $TEMP <<EOF
	<html>
	<head>
		<!--Load the AJAX API-->
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
		google.load('visualization', '1.0', {'packages':['corechart']});

		// On loading of google api
		google.setOnLoadCallback(drawChart);

		// Callback that creates and populates a data table,
		// instantiates the pie chart, passes in the data and
		// draws it.
		function drawChart() {

			// Create the data table.
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Title');
			data.addColumn('number', 'Value');

			data.addRows([
			$1
			['Place holder', 0]
			]);

			var options = {'title':'File sizes in kilobytes',
						'width':600,
						'height':600};

			// Passing created table to google visualization api
			var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
		</script>
	</head>

	<body>
		<!--Div holding a pie chart-->
		<div id="chart_div"></div>
	</body>
	</html>
EOF
# open default if not specified in arguments
	if [ -z "$ARG_BROWSER" ]; then
		xdg-open "$TEMP"
	else
		$ARG_BROWSER "$TEMP"
	fi
}


function get_directory_yad()
{
	DIRECT=$(yad --file --directory)
	if [ "$?" = "1" ]; then
		ARGS_OK="-1"
	fi 
}

function get_directory_zenity()
{
	DIRECT=$(zenity --file-selection --directory)
	if [ $? = "1" ]; then
		ARGS_OK="-1"
	fi 
}

function print_version()
{
	echo "$VERSION"
}

function process_files_into_dictionary()
{
	temp_sizes=$(mktemp)
	temp_files=$(mktemp)
	echo "$1" | cut -d "	" -f1 > $temp_sizes
	# for line in "$(cat fline.txt)" ;
	echo "$1" | rev | cut -d "/" -f 1 | rev > $temp_files

	num=1
	for line in $(<$temp_sizes) ;
	do
		# MYMAP["$(head -n$NUM files.txt | tail -n1)"]="$line"
		key="$(head -n$num $temp_files | tail -n1)"
		# echo "This is your key $KEY"
		MYMAP["${key}"]="${line}"
		num=$((num + 1))

	done
}


# Entry point

map_arguments $@


ARGS_OK=1


if [ -n "$ARG_HELP" ]; then
	help
	ARGS_OK="0"
else 
	if [ -n "$ARG_GUI" ]; then
		case "$ARG_GUI_PACK" in
		"zenity")
		get_directory_zenity
		;;
		"yad")
		get_directory_yad
		;;
		*)
		get_directory_yad
		;;
		esac

	elif [ -n "$ARG_DIRECT" ]; then
		$DIREC="$ARG_DIRECT"
	else
		ARGS_OK="-2"
	fi

	if [ -n "$ARG_VERSION" ]; then
			print_version
	fi



	if [ -n "$ARG_RECURSIVE"  ]; then
			WHOLE_STAT=$(find $DIRECT -type f -print0 | du --files0-from=- )
	else 
			WHOLE_STAT=$(du -d 1 -a $DIRECT)
			# echo "$WHOLE_STAT"
	fi
fi
# ARGS_OK="0"
if [ "$ARGS_OK" = "1" ]; then

	#last line of file is the one with size of current directory
	DIRECTORY_TOTAL=$(echo "$WHOLE_STAT" | tail -n 1)
	TOTAL_DIRECTORY_SIZE=$(echo "$DIRECTORY_TOTAL"| cut -d "	" -f 1 )

	# inplace deleting last line of f1.txt
	ALL_FILES=$(echo "$WHOLE_STAT" | sed '$ d')


	# 		unsetting map to prevent accidental persistence
	unset MYMAP	
	declare -A MYMAP

	process_files_into_dictionary "$ALL_FILES"

	#processing dictiornary into js data_row
	TABLEDATA=""
	for K in "${!MYMAP[@]}" ; 
	do 
		TABLEDATA+="['$K', ${MYMAP[$K]}],
		" 
	done
	${TABLEDATA::-3}

	create_chart "$TABLEDATA"


	unset MYMAP
else
echo "Error:"
	case $ARGS_OK in 
	"-1")
		echo "Operation cancelled"#>2
		;;
	"-2")
		echo "Directory path not provided"#>2
		;;
	*)
		echo "Unknown error"#>2
	esac
fi